package com.aklimov.image_resizer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;

class Main {

    private final static int MAX_WIDTH = 1024;
    private final static int MAX_HEIGHT = 600;

        private static ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 16, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());
//    private static ExecutorService executor = Executors.newSingleThreadExecutor();

    public static void main(String... args) {
//        if (args.length == 0) {
//            System.exit(1);
//            return;
//        }

        // TODO extract help constants
        if (contains(args, "-h") || contains(args, "--help")) {
            // TODO provide some help
            System.out.println("Some help info");

            System.exit(0);
            return;
        }

        final int inputPathIndex = indexOf(args, "--inputPath") + 1;
        final String inputPath = args[inputPathIndex];
        final int outputPathIndex = indexOf(args, "--outputPath") + 1;
        final String outputPath = args[outputPathIndex];

        // TODO ensure that pathes are different
        // TODO ensure that they are not empty
        // TODO ensure the same type

        final File intput = new File(inputPath);
        if (intput.isDirectory()) {
            resizeDir(inputPath, outputPath);
        } else {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        resizeImage(new File(inputPath), new File(outputPath));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        executor.shutdown();
    }

    private static void resizeDir(String path, String outputPath) {             
        final File dir = new File(path);
        if (dir.listFiles() == null) {
            return;
        }

        final File outputDir = new File(outputPath);
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                final File dstDir = new File(outputDir, file.getName());
                if (!dstDir.exists()) {
                    dstDir.mkdir();
                }
                resizeDir(file.getAbsolutePath(), dstDir.getAbsolutePath());
            } else if (isImageFile(file)) {
                executor.submit(() -> {
                    try {
                        resizeImage(file, new File(outputDir, file.getName()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    private static void resizeImage(File input, File output) throws IOException {
        final ImageReader imageReader = ImageIO.getImageReadersBySuffix(getExtension(input)).next();
        imageReader.setInput(new FileImageInputStream(input));
        final IIOMetadata metadata = imageReader.getImageMetadata(0);

        final BufferedImage image = imageReader.read(0);
        final int imageWidth = image.getWidth();
        final int imageHeight = image.getHeight();
        final int scaledWidth;
        final int scaledHeight;
        System.out.println("Image width: " + imageWidth + "; height: " + imageHeight);
        final float scaleX = MAX_WIDTH / (float) imageWidth;
        final float scaleY = MAX_HEIGHT / (float) imageHeight;
        if (scaleX < scaleY) {
            scaledWidth = MAX_WIDTH;
            scaledHeight = Math.round(imageHeight * scaleX);
        } else {
            scaledWidth = Math.round(imageWidth * scaleY);
            scaledHeight = MAX_HEIGHT;
        }
        System.out.println("Scaled width: " + scaledWidth + "; scaled height: " + scaledHeight);

        if (imageWidth <= scaledWidth && imageHeight <= scaledHeight) {
            return;
        }

        final Image tmp = image.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_SMOOTH);
        final BufferedImage scaledImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = scaledImage.createGraphics();
        graphics2D.drawImage(tmp, 0, 0, null);
        graphics2D.dispose();

        final File outputFile = new File(output.getParent(), getNameWithoutExtension(output) + ".jpg");
        final ImageWriter writer = ImageIO.getImageWriter(imageReader);
        writer.setOutput(new FileImageOutputStream(outputFile));
        final ImageWriteParam params = writer.getDefaultWriteParam();
        params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        params.setCompressionQuality(1F);
        if (params instanceof JPEGImageWriteParam) {
            ((JPEGImageWriteParam) params).setOptimizeHuffmanTables(true);
        }
        writer.write(null, new IIOImage(scaledImage, null, metadata), params);
        writer.dispose();
    }

    private static String getNameWithoutExtension(File file) {
        final String name = file.getName();
        return name.substring(0, name.lastIndexOf("."));
    }

    private static String getExtension(File file) {
        final String name = file.getName();
        return name.substring(name.lastIndexOf(".") + 1);
    }

    private static boolean isImageFile(File file) {
        final String name = file.getName();
        final String extension = name.substring(name.lastIndexOf(".") + 1);
        return extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg")/* || extension.equalsIgnoreCase("png")*/;
    }

    private static <T> boolean contains(T[] array, T elem) {
        for (T item : array) {
            if (item != null && item.equals(elem)) {
                return true;
            }
        }

        return false;
    }

    private static <T> int indexOf(T[] array, T elem) {
        for (int i = 0; i < array.length; i++) {
            final T item = array[i];
            if (item != null && item.equals(elem)) {
                return i;
            }
        }

        return -1;
    }
}
